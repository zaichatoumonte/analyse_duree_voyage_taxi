**PROJET D'ANALYSE DE LA DUREE DE VOYAGE EN TAXI BASEE SUR UN DATASET KAGGLE [TRIP_DURATION](https://www.kaggle.com/c/nyc-taxi-trip-duration/overview)**

Code source ecrit avec **Python Notebook**

***LANGAGE et BIBLIOTHEQUES UTILISEES***
- Python
- SciKitLean
- Pandas
- Numpy
- MatplotLib
- Seaborn
